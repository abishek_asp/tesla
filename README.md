**### General note: ###**

Micrium /OS-III is a Real Time Operating System (RTOS) and it has a preemption-based scheduling kernel. This academic TESLA project aims to develop a method to change the task management structure and scheduling policy of the OS using splay tree and binomial heap data structures. We implemented EDF Scheduling policy with Stack Resource Policy protocol for resource sharing and synchronization.

To know more details: Check our Phase 2 report and slides 
 in the repo location: Tesla/ce4053/Micrium/Report/